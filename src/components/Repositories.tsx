import Table from 'antd/lib/table';
import { useGetRepositories } from '../hooks/useGetRepositories';

const PAGE_SIZE = 10;

const Repositories = () => {
  const { repositories, isLoading, error, handlePageChange, totalPages, currentPage } =
    useGetRepositories(PAGE_SIZE);

  const columns = [
    {
      title: 'Repository name',
      dataIndex: 'nameWithOwner',
      key: 'nameWithOwner',
    },
    {
      title: 'Stars',
      dataIndex: 'stargazers',
      key: 'stargazers',
      render: (text: string) => <>🌟 {text}</>,
    },
    {
      title: 'Forks',
      dataIndex: 'forks',
      key: 'forks',
      render: (text: string) => <>🍴 {text}</>,
    },
  ];

  if (error) return <p>Error :(</p>;

  return repositories.length ? (
    <Table
      columns={columns}
      dataSource={repositories}
      pagination={{
        pageSize: PAGE_SIZE,
        total: totalPages,
        current: currentPage,
        onChange: handlePageChange,
        simple: true,
      }}
      loading={isLoading}
    />
  ) : null;
};

export default Repositories;
