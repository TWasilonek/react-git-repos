import { useEffect, useState, useCallback, useMemo } from 'react';
import { gql, useQuery } from '@apollo/client';

interface Repository {
  id: string;
  nameWithOwner: string;
  stargazers: {
    totalCount: number;
  };
  forks: {
    totalCount: number;
  };
}

interface RepositoriesQueryResults {
  search: {
    repositoryCount: number;
    edges: Array<{
      cursor: string;
      node: Repository;
    }>;
  };
}

interface ProcessedRepository {
  stargazers: number;
  forks: number;
  key: string;
  nameWithOwner: string;
  cursor: string;
}

const QUERY = gql`
  query GetRepositories($query: String!, $first: Int!, $after: String) {
    search(query: $query, type: REPOSITORY, first: $first, after: $after) {
      repositoryCount
      edges {
        cursor
        node {
          ... on Repository {
            id
            nameWithOwner
            stargazers {
              totalCount
            }
            forks {
              totalCount
            }
          }
        }
      }
    }
  }
`;

export const useGetRepositories = (pageSize: number) => {
  const [repositories, setRepositories] = useState<ProcessedRepository[]>([]);
  const [isRefetching, setIsRefetching] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [nextPage, setNextPage] = useState(1);
  const [totalLoaded, setTotalLoaded] = useState(0);
  const { loading, error, data, fetchMore } = useQuery<RepositoriesQueryResults>(QUERY, {
    variables: { query: 'react', first: pageSize },
  });

  const mapDataToRepositories = (nodes: { node: Repository; cursor: string }[]) =>
    nodes.length
      ? nodes.map(({ node, cursor }) => {
          return {
            nameWithOwner: node.nameWithOwner,
            stargazers: node.stargazers.totalCount,
            forks: node.forks.totalCount,
            key: node.id,
            cursor,
          };
        })
      : [];

  useEffect(() => {
    if (!loading && data) {
      const processedData = data ? mapDataToRepositories(data.search.edges) : [];
      setRepositories(processedData);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  useEffect(() => {
    if (nextPage !== currentPage) {
      setCurrentPage(nextPage);
      setTotalLoaded(repositories.length);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [nextPage, repositories]);

  const handlePageChange = useCallback(
    async (page) => {
      const nextDataIsAlreadyLoaded = page * pageSize <= totalLoaded;

      if (page < currentPage || (page > currentPage && nextDataIsAlreadyLoaded)) {
        setNextPage(page);
        return;
      }

      const { cursor } = repositories[repositories.length - 1];
      if (!cursor) {
        return;
      }

      setNextPage(page);
      setIsRefetching(true);
      try {
        const response = await fetchMore({
          variables: {
            after: cursor,
          },
        });
        const newRepositories = response.data?.search?.edges
          ? repositories.concat(mapDataToRepositories(response.data.search.edges))
          : repositories;
        setRepositories(newRepositories);
      } catch (e) {
        console.error(e);
      } finally {
        setIsRefetching(false);
      }
    },
    [currentPage, fetchMore, pageSize, repositories, totalLoaded]
  );

  const isLoading = useMemo(() => {
    return loading || isRefetching;
  }, [loading, isRefetching]);

  const totalPages = useMemo(() => {
    return data ? Math.ceil(data.search.repositoryCount / pageSize) : 0;
  }, [data, pageSize]);

  return {
    repositories,
    isLoading,
    error,
    handlePageChange,
    totalPages,
    currentPage,
  };
};
