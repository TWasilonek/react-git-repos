import Layout, { Content, Header } from 'antd/lib/layout/layout';
import styled from 'styled-components';

import './App.css';
import Repositories from './components/Repositories';

const StyledHeader = styled(Header)`
  padding-left: 15px;
  padding-right: 15px;
`;

const Title = styled.h2`
  color: white;
`;

const StyledContent = styled(Content)`
  padding: 15px;
`;

function App() {
  return (
    <Layout>
      <StyledHeader>
        <Title>Github repositories</Title>
      </StyledHeader>
      <StyledContent>
        <Repositories />
      </StyledContent>
    </Layout>
  );
}

export default App;
